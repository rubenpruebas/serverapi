package com.example.pruebaspring1.services;

import com.example.pruebaspring1.models.Job;
import com.example.pruebaspring1.repositories.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JobService {
    @Autowired
    private JobRepository jobRepository;

    public List<Job> getAllJobs() {
        return jobRepository.findAll();
    }

    public Job getJobById(String id) {
        if (id == null) return null;
        return jobRepository.findById(id).orElse(null);
    }

}
