package com.example.pruebaspring1.services;

import com.example.pruebaspring1.models.Employee;
import com.example.pruebaspring1.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    public Employee getEmployeeById(Long id) {
        return employeeRepository.findById(id).orElse(null);
    }

    public Employee addEmployee(Employee e) {
        return employeeRepository.save(e);
    }

    public Employee updateEmployee(Long id, Employee e) {
        Employee employee = employeeRepository.findById(id).orElse(null);
        if (employee != null) {
            employee.setFirst_name(e.getFirst_name());
            employee.setLast_name(e.getLast_name());
            employee.setEmail(e.getEmail());
            employee.setPhone_number(e.getPhone_number());
            employee.setHire_date(e.getHire_date());
            employee.setJob_id(e.getJob_id());
            employee.setSalary(e.getSalary());
            employee.setCommission_pct(e.getCommission_pct());
            employee.setManager_id(e.getManager_id());
            employee.setDepartment_id(e.getDepartment_id());
            return employeeRepository.save(employee);
        }
        return null;
    }

    public void deleteEmployee(Long id) {
        employeeRepository.deleteById(id);
    }
}
