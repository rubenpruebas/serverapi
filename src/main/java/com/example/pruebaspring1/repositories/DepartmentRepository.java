package com.example.pruebaspring1.repositories;

import com.example.pruebaspring1.models.Department;
import com.example.pruebaspring1.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {
}
