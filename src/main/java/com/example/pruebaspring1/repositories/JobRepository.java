package com.example.pruebaspring1.repositories;

import com.example.pruebaspring1.models.Department;
import com.example.pruebaspring1.models.Job;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobRepository extends JpaRepository<Job, String> {
}
