package com.example.pruebaspring1.controllers;

import com.example.pruebaspring1.models.Department;
import com.example.pruebaspring1.models.Employee;
import com.example.pruebaspring1.services.DepartmentService;
import com.example.pruebaspring1.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/departments")
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @GetMapping
    public List<Department> getAllDepartments() {
        return departmentService.getAllDepartments();
    }

    @GetMapping("/{id}")
    public Department getDepartmentById(@PathVariable Long id) {
        Department department = departmentService.getDepartmentById(id);
        return (department != null) ? department : new Department();
    }

}
