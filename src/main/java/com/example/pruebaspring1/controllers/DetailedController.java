package com.example.pruebaspring1.controllers;

import com.example.pruebaspring1.models.Department;
import com.example.pruebaspring1.models.Detailed;
import com.example.pruebaspring1.models.Employee;
import com.example.pruebaspring1.services.DepartmentService;
import com.example.pruebaspring1.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/detailed")
public class DetailedController {
    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/{id}")
    public Detailed getDetailedById(@PathVariable Long id) {
        Employee employee = employeeService.getEmployeeById(id);
        if (employee == null) return new Detailed();
        return new Detailed(employee, departmentService.getDepartmentById(employee.getDepartment_id()));
    }
}
